const express = require("express");
const bodyParser = require("body-parser");
// var mongo = require('mongodb');
var MongoClient = require('mongodb').MongoClient;
const app = express();
var http = require('http');
const cors = require('cors');
var server = http.createServer(app);

app.use(bodyParser.json({limit: '50mb', extended: true}));
app.use(bodyParser.json({type: 'application/json'}));

app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(cors());

app.get('/', function (req, res) {
    res.send("Hello ");
})

const routing = require('./route');
app.use('/api', routing);







// parse requests of content-type - application/x-www-form-urlencoded
// app.use(bodyParser.urlencoded({ extended: true }));



const PORT = 3000;
server.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});


