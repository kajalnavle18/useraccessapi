const userController = require('./controller/useracess');
const middleware = require('./middleware');
const express = require('express');
const router = express.Router();


router.post("/userregister",middleware.checkToken,userController.userRegister);
router.get("/listOfUser/:id",userController.listOfUser);
router.post("/editUser",userController.editUser);
router.get("/deleteUser/:id/:sts",userController.deleteUser);
router.post("/loginUser",userController.loginUser)
router.get("/listofIndustry",userController.listofIndustry);
router.get("/listofIndustryrole/:id",userController.listofIndustryrole);


module.exports = router;

