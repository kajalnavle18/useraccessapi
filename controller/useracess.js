'use strict';

var db = require('../config_db/db');
var conn = require("../config_db/dbconnection");
var connection = conn.getConnection();
var nodemailer = require('nodemailer');
let jwt = require('jsonwebtoken');

module.exports.userRegister = function (req, res) {
    try {
        let query;
        let valuesarr=[];
if(req.body.id == 'undefined'){
    query = `INSERT INTO mstuserregister (name,address,password,industry,role,email) VALUES(?,?,?,?,?,?)`;
    valuesarr.push(req.body.name, req.body.address, req.body.password,req.body.industry,req.body.role,req.body.email);
}else{
    console.log("else")
    query = `UPDATE mstuserregister SET name=?,address=?,password=?,industry=?,role=?,email=?  WHERE id =?`;
    valuesarr.push(req.body.name, req.body.address, req.body.password,req.body.industry,req.body.role,req.body.email,req.body.id);
}
      
        connection.query(query, valuesarr,function (error, result) {
            if (error)
                throw error;
            else {
                if (result) {
                    var transporter = nodemailer.createTransport({
                        service: 'gmail',
                        auth: db.auth
                      });
                      
                      var mailOptions = {
                        from: db.auth.user,
                        to: req.body.email,
                        subject: db.mailOptions.subject,
                        text: db.mailOptions.text
                      };

                      transporter.sendMail(mailOptions, function(error, info){
                        if (error) {
                          console.log(error);
                          res.send({success:false,msg:`Email not send to ${req.body.email}`})
                        } else {
                            res.send({success:true,msg:`Email send successfully ${req.body.email} AND Data Insert successfully` });
                          console.log('Email sent: ' + info.response);
                        }
                      });
                } else {
                    res.send({ success: false, msg: "Data Not Found" });
                }
            }
        })
    } catch (e) {
        res.send({ success: false, msg: "Something Went Wrong", payload: e });
    }
};


module.exports.listOfUser = function (req, res) {
    try {
        let query;
        if(req.params.id != 0){
            query = `SELECT * FROM mstuserregister WHERE sts = 1 AND id = ${req.params.id}` ;  
        }else{
            query = `SELECT * FROM mstuserregister WHERE sts = 1`;
        }
       
        connection.query(query,function (error, result) {
            if (error)
                throw error;
            else {
                if (result) {
                    res.send({success:true,msg:"Data Fetech Successfully",payload:result})
        
                } else {
                    res.send({ success: false, msg: "Data Not Found" });
                }
            }
        })
    } catch (e) {
        res.send({ success: false, msg: "Something Went Wrong", payload: e });
    }
};

module.exports.editUser = function (req, res) {
    try {
        let query;
        let updatearr=[];
        query = `UPDATE mstuserregister SET name=?,address=?,password=?,industry=?,role=?,email=?  WHERE id =?`;
        updatearr.push(req.body.name, req.body.address, req.body.password,1,1,req.body.email,req.body.id);
        connection.query(query,function (error, result) {
            if (error)
                throw error;
            else {
                if (result) {
                    res.send({success:true,msg:"Data Fetech Successfully"})
        
                } else {
                    res.send({ success: false, msg: "Data Not Found" });
                }
            }
        })
    } catch (e) {
        res.send({ success: false, msg: "Something Went Wrong", payload: e });
    }
};

module.exports.deleteUser = function (req, res) {
    try {
        let query;
        let deletearr=[];
        query = `UPDATE mstuserregister SET sts =?  WHERE id =?`;
        deletearr.push(req.params.sts,req.params.id);
        connection.query(query,deletearr,function (error, result) {
            if (error)
                throw error;
            else {
                if (result) {
                    res.send({success:true,msg:"Data Delete Successfully"})
        
                } else {
                    res.send({ success: false, msg: "Data Not Found" });
                }
            }
        })
    } catch (e) {
        res.send({ success: false, msg: "Something Went Wrong", payload: e });
    }
};

module.exports.loginUser = function (req, res) {
    try {
        let query;
        // let username =req.body.name;
        let loginarr=[];
        query = `SELECT * FROM mstuserregister WHERE name =? AND password =?`;
        loginarr.push(req.body.name,req.body.password);
        connection.query(query,loginarr,function (error, result) {
            if (error)
                throw error;
            else {
                if (result.length > 0) {
                     // return the JWT token for the future API calls
                     result[0].auth_token = jwt.sign({username: req.body.name},
                        db.Sqldb.secreatekey,
                        {
                            expiresIn: '1h' // expires in 1 hours
                        })
                    res.send({success:true,msg:"Login Successfully",payload:result})
        
                } else {
                    res.send({ success: false, msg: "Data Not Found" });
                }
            }
        })
    } catch (e) {
        res.send({ success: false, msg: "Something Went Wrong", payload: e });
    }
};

module.exports.listofIndustry = function (req, res) {
    try {
        let query;
            query = `SELECT * FROM mstindustry` ;  
       
        connection.query(query,function (error, result) {
            if (error)
                throw error;
            else {
                if (result) {
                    res.send({success:true,msg:"Data Fetech Successfully",payload:result})
        
                } else {
                    res.send({ success: false, msg: "Data Not Found" });
                }
            }
        })
    } catch (e) {
        res.send({ success: false, msg: "Something Went Wrong", payload: e });
    }
};

module.exports.listofIndustryrole = function (req, res) {
    try {
        let query;
            query = `SELECT industry.industry_id,industry.industry_name,roles.role_id,roles.role_name,roles.role_industry_id
            FROM mstindustry AS industry  JOIN mstrole AS roles  
            ON industry.industry_id =  roles.role_industry_id WHERE industry.industry_id = ${req.params.id} ` ;  
       
        connection.query(query,function (error, result) {
            if (error)
                throw error;
            else {
                if (result) {
                    res.send({success:true,msg:"Data Fetech Successfully",payload:result})
        
                } else {
                    res.send({ success: false, msg: "Data Not Found" });
                }
            }
        })
    } catch (e) {
        res.send({ success: false, msg: "Something Went Wrong", payload: e });
    }
};



module.exports.treeView = function (req, res) {
    try {
        let query;
            query = `SELECT industry.industry_id as indid,industry.industry_name,roles.role_id,roles.role_name,roles.role_industry_id,roles.display_order,
            userrgister.name,userrgister.address,userrgister.industry,userrgister.email FROM mstuserregister AS userrgister JOIN 
                         mstindustry AS industry  ON industry.industry_id = userrgister.industry
                         JOIN mstrole AS roles
                        ON industry.industry_id =  roles.role_industry_id WHERE industry.industry_id = ${req.params.id} ORDER BY roles.display_order` ;  
       console.log("query-->",query);
        connection.query(query,function (error, result) {
            if (error)
                throw error;
            else {
                // const data =getDefaultUniqueValue(result);
                //         console.log("data------------------->",data);
                if (result) {
                    console.log("data-->",result);
                    const data =getDefaultUniqueValue(result);
                    console.log("data------------------->",data);
                   var  subUniqueValue = data;
                    let subTempArr = [];
                    console.log("=>",subUniqueValue);
                    for (let k in subUniqueValue) {
                    console.log("===?",subUniqueValue[k]["role_id"])
                        let tempObjSub = {};
                        tempObjSub.industry_id = subUniqueValue[k].indid;
                        tempObjSub.industry_name = subUniqueValue[k].industry_name;
                        tempObjSub.role_id = subUniqueValue[k].role_id;
                        tempObjSub.role_name = subUniqueValue[k].role_name;
                        tempObjSub.role_industry_id = subUniqueValue[k].role_industry_id;
                        tempObjSub.role_display_order= subUniqueValue[k].display_order;
                        tempObjSub.role_parent_id = subUniqueValue[k].parent_id;
                   
                        let unit_arr_sub = [];
                        for (let l in result) {
                            if (subUniqueValue[k].menu_id === result[l].menu_id) {
                                    unit_arr_sub.push({
                                        role_id: result[l].role_id,
                                        role_name: result[l].role_name,
                                        role_industry_id: result[l].role_industry_id
                                    });
                               
                            }
                        }
                        tempObjSub.menu_data = unit_arr_sub;
                        subTempArr.push(tempObjSub);
                        console.log("unitarr-->",subTempArr);
                    }
                    let not_parent = subTempArr.filter((item) => {
                        return item.role_parent_id === 0;
                    });
    
                    let with_parent = subTempArr.filter((item) => {
                        return item.role_parent_id !== 0;
                    });
                    let subParentTempArr = [];
                if (with_parent.length > 0) { 
                    const subParentUniqueValue = with_parent;
                    for (let k in subParentUniqueValue) {
                        console.log("subp------>",subParentUniqueValue);
                        let tempObjSub2 = {};

                       
                        tempObjSub2.industry_id = subParentUniqueValue[k].industry_id;
                        tempObjSub2.industry_name = subParentUniqueValue[k].industry_name;
                        tempObjSub2.role_id = subParentUniqueValue[k].role_id;
                        tempObjSub2.role_name = subParentUniqueValue[k].role_name;
                        tempObjSub2.role_industry_id = subParentUniqueValue[k].role_industry_id;
                        tempObjSub2.role_display_order = subParentUniqueValue[k].role_display_order;
                        tempObjSub2.role_parent_id = subParentUniqueValue[k].parent_id;
                     
                        let unit_arr_sub_2 = [];
                        for (let l2 in with_parent) {
                            if (subParentUniqueValue[k].parent_id === with_parent[l2].role_parent_id) {
                                unit_arr_sub_2.push(with_parent[l2]);
                            }
                        }
                        console.log("unit_arr_sub_2------>",unit_arr_sub_2);
                        tempObjSub2.menu_data = unit_arr_sub_2;
                        subParentTempArr.push(tempObjSub2);
                    }
                }
             console.log("=====>",subParentTempArr);
                    res.send({success:true,msg:"Data Fetech Successfully",payload:subParentTempArr})
        
                } else {
                    res.send({ success: false, msg: "Data Not Found" });
                }
            }
        })
    } catch (e) {
        res.send({ success: false, msg: "Something Went Wrong", payload: e });
    }
};