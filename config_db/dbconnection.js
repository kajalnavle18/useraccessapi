
var mysql = require("mysql");
var prop = require("./db");
var connection;
module.exports = {
    getConnection: () => {
        connection = mysql.createConnection({
            host: prop.Sqldb.host,
            user: prop.Sqldb.user,
            password: prop.Sqldb.password,
            database: prop.Sqldb.database,
            multipleStatements: true
        });
        connection.connect(function onConnect(err) {   // The server is either down
            if (err) {                                  // or restarting (takes a while sometimes).
                console.log('error when connecting to db:', err);
                setTimeout(getConnection, 10000);    // We introduce a delay before attempting to reconnect,
            }                                           // to avoid a hot loop, and to allow our node script to
        });                                             // process asynchronous requests in the meantime.
        connection.on('error', function onError(err) {
            console.log('db error', err);
            if (err.code == 'PROTOCOL_CONNECTION_LOST') {   // Connection to the MySQL server is usually
                getConnection();                         // lost due to either server restart, or a
            } else {                                        // connnection idle timeout (the wait_timeout
                throw err;                                  // server variable configures this)
            }
        });
        return connection;
    }
   
};
